package com.example.activityforresult;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_CODE = 10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void CallSecondActivity(View view) {
        EditText name = findViewById(R.id.etName);
        EditText phone = findViewById(R.id.etAge);
        String stringName = name.getText().toString();
        String stringPhone = phone.getText().toString();
        Intent i = new Intent(this, SecondActivity.class);
        i.putExtra("yourname", stringName);
        i.putExtra("yourphone",stringPhone);
        startActivityForResult(i, REQUEST_CODE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
            if (data.hasExtra("returnkey")) {
                String result = data.getExtras().getString("returnkey");
                if (result != null && result.length() > 0) {
                    Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
                }
            }
        }
        else if (resultCode == RESULT_CANCELED && requestCode == REQUEST_CODE) {
            Toast.makeText(this, data.getExtras().getString("returnkey"),
                    Toast.LENGTH_SHORT).show();
        }
    }
}